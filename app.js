var fs = require('fs'),
    path = require('path'),
    xml2js = require('xml2js'),
    parser = new xml2js.Parser(),
    builder = new xml2js.Builder(),
    argv = require('optimist').demand(['file']).argv,
    EventEmitter = require("events").EventEmitter;
	ee = new EventEmitter(),
    configuration = require('./config-processing'),
    libraryComponents = require('./library'),
	filename = argv.file;

fs.readFile(__dirname + filename, function(err, data) {
    if (err) return console.log(err);
    parser.parseString(data, xmlDocumentProcessing);
});

function xmlDocumentProcessing(err, xml) {
	if (err) return console.log(err);
	
	var products = xml.catalog['product'];

	products.forEach(function (product) {
		var customAttributesNodes = product['custom-attributes'] || [{'custom-attribute' : []}],
			customAttributes = customAttributesNodes[0]['custom-attribute'];

		if (!customAttributes || !customAttributes.length) {
			return;
		}

		customAttributes.forEach(function (attribute, index, array) {
			var attributeID = attribute['$']['attribute-id'],
				attributeValue = attribute['_'],
				callback = configuration.customAttributes[attributeID];
			
			if ('function' !== typeof callback) {
				return;
			}

			attribute = callback.apply(xml, [attributeValue, attributeID, {
				'attribute' : attribute,
				'product' : product,
				'xml' : xml
			}]);

			if (attribute === null) {
				return delete array[index];
			}

			array[index] = attribute;
		}, product);

	}, xml.catalog);

	ee.emit('processed', {
		'document' : xml
	});
}

ee.on('processed', function (data) {
	var processedFileName = filename.replace('.xml', '-processed.xml'),
		xmlObject = builder.buildObject(data['document']).toString({ 
			pretty: true, 
			indent: '  ', 
			offset: 1, 
			newline: '\n' 
		});
	
	fs.writeFile(__dirname + processedFileName, xmlObject, function (err) {
		if (err) return console.log(err);
		ee.emit('saved', {
			'filename' : processedFileName
		});
	});
});

ee.on('processed', function (data) {
	var processedFileName = filename.replace('.xml', '-productcontent.xml'),
		productContent = configuration['productContent'],
		xml = libraryComponents.document,
		content = libraryComponents.content;

	for (var attributeID in productContent['data']) {
		if (!(attributeID in productContent['mapping'])) {
			console.warn('Missed Product Content Attribute mapping for "' + attributeID + '"')
			continue;
		}

		productContent['data'][attributeID].forEach(function (contentData) {
			var contentNode = JSON.parse(JSON.stringify(content)); // replace with require('clone')
				contentID = productContent['mapping'][attributeID] + '-' + contentData['productID'],
				contentBody = buildCustomAttribute('body', {
					'xml:lang' : 'x-default'
				}, contentData['content']);

			contentNode['$']['content-id'] = contentID;
			contentNode['display-name']['_'] = contentData['title'];
			contentNode['custom-attributes']['custom-attribute'].push(contentBody);

			xml['library']['content'].push(contentNode);
		});
	}

	fs.writeFile(__dirname + processedFileName, builder.buildObject(xml).toString({ 
		pretty: true, 
		indent: '  ', 
		offset: 1, 
		newline: '\n' 
	}), function (err) {
		if (err) return console.log(err);
		ee.emit('saved', {
			'filename' : processedFileName
		});
	});
});

ee.on('saved', function (data) {
	console.log('Processed XML file successfully saved in ' + data['filename']);
});



/**
 *
 */
function buildCustomAttribute(attributeID, attributes, data) {
	var attribute = {
		'$' : {},
		'_' : ''
	};

	if (!attributeID) {
		return attribute;
	}

	if (data && 'string' !== typeof data) {
		attributeID = data['attributeID'] || attributeID;
		attribute['_'] = data['value'];
	} else {
		attribute['_'] = data;
	}

	attribute['$']['attribute-id'] = attributeID;

	for (var key in attributes) {
		attribute['$'][key] = attributes[key];
	};

	return attribute;
}
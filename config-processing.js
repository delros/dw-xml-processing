var customAttributesMapping = {
	'ProductVideo' : productVideoContentPush,
	'kiehlsSEO' : productContentPush,
	'productRoutines' : productContentPush,
	'kiehlsTips' : extendTipsTab,
	'kiehlsExplore' : extendTipsTab,
	'insiderImage' : productInsiderContentPush,
	'insiderName' : productInsiderContentPush,
	'insiderQuote' : productInsiderContentPush,
	'productQuote' : productQuoteContentPush,
	'productQuoteSource' : productQuoteContentPush,
	'tipsCarousel' : productContentPush,
	'tipsVideo' : productContentPush
};

var productContent = {};

var productContentMapping = {
	'kiehlsSEO' : 'pdp-content-seo',
	'ProductVideo' : 'pdp-content-video',
	'productRoutines' : 'pdp-content-routines',
	'insiderImage' : 'pdp-content-insiders',
	'insiderName' : 'pdp-content-insiders',
	'insiderQuote' : 'pdp-content-insiders',
	'productQuote' : 'pdp-content-quotes',
	'productQuoteSource' : 'pdp-content-quotes',
	'tipsCarousel' : 'pdp-content-tipscarousel',
	'tipsVideo' : 'pdp-content-tipsvideo'
};


/**
 * @alias The shortcut for "multiplyProductContentPush"
 * with changed the specific options for the attributes set
 */
function productInsiderContentPush(value, id, data) {
	data['count'] = 3;
	data['type'] = 'insiderquote';
	return multiplyProductContentPush(value, id, data);
}

/**
 * @alias The shortcut for "multiplyProductContentPush"
 * with changed the specific options for the attributes set
 */
function productQuoteContentPush(value, id, data) {
	data['count'] = 2;
	data['type'] = 'productquote';
	return multiplyProductContentPush(value, id, data);
}

var _tempStore = {};
var valueWrapper = '<div class="migration-fallback-%id%">%value%</div>';
function multiplyProductContentPush(value, id, data) {
	var attribute = data['attribute'],
		productID = data['product']['$']['product-id'],
		valueFormatted = valueWrapper;

	if (!(productID in _tempStore)) {
		_tempStore[productID] = {};
	}

	if (!(data['type'] in _tempStore[productID])) {
		_tempStore[productID][data['type']] = [];
	}

	valueFormatted = valueFormatted.replace('%id%', id);
	valueFormatted = valueFormatted.replace('%value%', value || '');

	_tempStore[productID][data['type']].push(valueFormatted);

	if (_tempStore[productID][data['type']].length < data['count']) {
		return attribute;
	}
	
	value = _tempStore[productID][data['type']].join('');
	delete _tempStore[productID][data['type']];

	return productContentPush(value, id, data);
}

/**
 * TBD
 * @param value {String}
 * @param id {String}
 * @param data {Object}
 */
function productContentPush(value, id, data) {
	var attribute = data['attribute'],
		productID = data['product']['$']['product-id'],
		productTitle = data['product']['display-name'][0]['_'];

	if (!(id in productContent)) {
		productContent[id] = [];
	}

	if ('attributeContent' in data) {
		value = {
			'attributeID' : data['attributeContent'],
			'value' : value
		}
	}

	productContent[id].push({
		'productID' : productID,
		'title' : productTitle,
		'content' : value
	});

	return attribute;
}

/**
 * TBD
 * @param value {String}
 * @param id {String}
 * @param data {Object}
 */
function productVideoContentPush(value, id, data) {
	var attribute = data['attribute'],
		parsedValue = /img id="(.*)" class/.exec(value);

	if (parsedValue && parsedValue.length) {
		value = parsedValue[1];
	}

	data['attributeContent'] = 'videoId';

	return productContentPush(value, id, data);
}

/**
 * TBD
 * @param value {String}
 * @param id {String}
 * @param data {Object}
 */
function extendTipsTab(value, id, data) {

	var attributeTipsTab = getCustomAttribute('tipsTab', data['product']);
	
	if (id === 'kiehlsTips') {
		attributeTipsTab['_'] = value + attributeTipsTab['_'];
	} else {
		attributeTipsTab['_'] += value;
	}

	return null;
}

/**
 * TBD
 * @param id {String}
 * @param product {Object}
 */
function getCustomAttribute(id, product) {
	var customAttributesNodes = product['custom-attributes'] || [{'custom-attribute' : []}],
		customAttributes = customAttributesNodes[0]['custom-attribute'],
		result = {
			'$' : {
				'attribute-id' : id
			},
			'_' : ''
		},
		isAttributeExist = false;

	if (!customAttributes || !customAttributes.length) {
		return;
	}

	customAttributes.forEach(function (attribute, index, array) {
		if (attribute && attribute['$']['attribute-id'] === id) {
			isAttributeExist = true;
			result = attribute;
			return;
		}
	});

	if (!isAttributeExist) {
		customAttributes.push(result);
	}

	return result;
}


module.exports = {
	'customAttributes' : customAttributesMapping,
	'productContent' : {
		'mapping' : productContentMapping,
		'data' : productContent
	}
};